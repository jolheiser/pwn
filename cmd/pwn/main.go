package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"go.jolheiser.com/pwn"
)

var (
	client = pwn.New(pwn.WithHTTP(&http.Client{
		Timeout: time.Second * 10,
	}))

	Version = "develop"
)

func main() {
	paddingFlag := flag.Bool("padding", false, "Add padding to password checks - more secure, but slower")
	helpFlag := flag.Bool("help", false, "Show the help dialog")
	versionFlag := flag.Bool("version", false, "Show the version of pwn")
	flag.Parse()

	if *helpFlag {
		help()
		os.Exit(0)
	}
	if *versionFlag {
		version()
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) == 0 {
		logError("pwn needs a sub-command")
	}

	switch args[0] {
	case "help":
		help()
	case "version":
		version()
	case "password", "pw":
		checkPassword(args, *paddingFlag)
	default:
		logError("unknown sub-command")
	}
}

// If used in CI of any kind
// Exits with error code 1 if password has been pwned
// Exits with error code 0 if password has not been pwned
func checkPassword(args []string, padding bool) {
	if len(args) == 1 {
		logError("password must be supplied")
	}

	count, err := client.CheckPassword(args[1], padding)
	if err != nil {
		panic(err)
	}

	fmt.Printf("This password has been pwned %d times.\n", count)
	if count > 0 {
		os.Exit(1)
	}
	os.Exit(0)
}

func logError(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}

func help() {
	fmt.Println(strings.Trim(fmt.Sprintf(`
pwn version %s

pwn is a command-line tool to test a variety of things against https://haveibeenpwned.com

Commands:
	help         - Print this dialog
	version      - Check the current version of pwn
	password, pw - Check if a password has been pwned

Flags:
	--help    - Print this dialog
	--version - Check the current version of pwn
	--padding - Adds padding to password checks. More secure, but slower.
`, Version), "\n"))
}

func version() {
	fmt.Printf("pwn version: %s\n", Version)
}
