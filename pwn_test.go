package pwn

import (
	"math/rand"
	"net/http"
	"os"
	"testing"
	"time"
)

var client = New(WithHTTP(&http.Client{
	Timeout: time.Second * 2,
}))

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())
	os.Exit(m.Run())
}
